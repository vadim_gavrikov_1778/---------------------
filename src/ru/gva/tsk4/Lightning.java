package ru.gva.tsk4;

import java.util.Scanner;

/**
 * Расчет расстояния до места удара молнии.
 * Звук в воздухе распространяется со скоростью
 * приблизительно равной 1234,8 километров в час.
 * Зная интервал времени между вспышкой молнии и
 * звуком сопровождающим ее можно рассчитать расстояние.
 * Допустим интервал 6,8 секунды.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Lightning {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        double v = 1234.8;
        double t;
        System.out.println("Введите временной промежуток между вспышкой молнии и громом: ");
        t = sc.nextDouble();
        double s = (t / 360) * v;
        System.out.println("Расстояние до удара молнии: " + s + " километра");
    }
}
