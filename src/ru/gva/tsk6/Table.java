package ru.gva.tsk6;

import java.util.Scanner;

/**
 * Напишите программу, которая будет выводить таблицу умножения
 * введенного пользователем числа с клавиатуры.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Table {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int table = sc.nextInt();
        for (int i = 1; i <= table; i++) {
            System.out.println(table + " * " + i + " = " + table * i);

        }
    }
}
