package ru.gva.tsk14;

import java.util.Scanner;

/**
 * Определить является ли символ введенный с клавиатуры цифрой,
 * буквой или знаком пунктуации.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Determine {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String string = sc.nextLine();
        char number = string.charAt(0);
        if (Character.isDigit(number)) {
            System.out.print("Число!");
        }
        if (Character.isLetter(number)) {
            System.out.print("Буква!");
        }
        if (",.:;?!".contains(string)) {
            System.out.print("Пунктуационный знак!");
        }
    }
}
