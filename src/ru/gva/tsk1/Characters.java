package ru.gva.tsk1;

import java.util.Scanner;

/**
 * Напишите программу, которая считывает символы пока не встретится точка.
 * Также предусмотрите вывод количества пробелов.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Characters {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String characters = sc.nextLine();
        System.out.println("Кол-во пробелов в строчке " + space(characters));
        System.out.println("Кол-во символов до точки " + point(characters));
    }

    /**
     * Метод подсчета символов до точки.
     *
     * @param characters Строка
     * @return кол-во символов до '.'
     */
    private static int point(String characters) {
        int point = 0;
        for (int i = 0; i < characters.length(); i++) {
            if (characters.charAt(i) == '.') {
                point = i;

                break;
            }
        }
        return point;
    }

    /**
     * Метод для подсчета пробелов в строчке.
     *
     * @param characters строчка
     * @return кол-во ' ' в строке.
     */
    private static int space(String characters) {
        int space = 0;
        for (int i = 0; i < characters.length(); i++) {
            if (characters.charAt(i) == ' ') {
                space++;
            }
        }
        return space;
    }


}
