package ru.gva.tsk7;

import java.util.Scanner;

/**
 * Создайте простую игру основанную на угадывании чисел.
 * Пользователь должен угадать загаданное число введя его в консоль.
 * Если пользователь угадал число, то программа выведет «Правильно» и игра закончится, если нет,
 * то пользователь продолжит вводить числа.
 * Вывести «Загаданное число больше»- если пользователь ввел число меньше загаданного,
 * «Загаданное число меньше»- если пользователь ввел число больше загаданного.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Games {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int number = 0 + (int) (Math.random()*(20));
        for (int i = 0; i<3; i++){
            System.out.println("Введите число");
            int number1 = sc.nextInt();
            if (number1 == number) {
                System.out.println("Правильно");
                break;
            }else {
                System.out.println("Вы не угадали!!!");
                if (number1 > number){
                    System.out.println("Загаданое число меньше");
                }else {
                    System.out.println("Загаданное число больше");
                }
                System.out.println("Попробуйте ещё");
                System.out.println("PS: у вас осталось " + (3-(i+1)));
                if ((3-(i+1))==0){
                    System.out.println("У вас закончилсь попытки");
                    System.out.println("Загаданное число равно - " +number);
                }
            }
        }
    }

}
