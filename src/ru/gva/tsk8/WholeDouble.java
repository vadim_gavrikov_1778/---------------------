package ru.gva.tsk8;

import java.util.Scanner;

/**
 * Напишите программу, которая будет проверять является ли число типа double целым.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class WholeDouble {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        double number = sc.nextDouble();
        if (number - whole(number) == 0){
            System.out.println("Число целое");
        }else {
            System.out.println("Число не целое");
        }
    }

    /**
     * Данный метод преобразует число типа double в число типа int.
     *
     * @param number число которое нужно преобразовать
     * @return возвращает число без знаков после запятой.
     */
    private static int whole(double number) {
        return (int) number;
    }
}
