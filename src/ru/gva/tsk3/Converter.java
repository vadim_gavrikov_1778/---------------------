package ru.gva.tsk3;

import java.util.Scanner;

/**
 * Напишите метод для перевода рублей в евро по заданному курсу.
 * В качестве аргументов передайте количество рублей и курс.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Converter {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во рублей - ");
        double rubles = sc.nextDouble();
        System.out.println("Введите курс обмена - ");
        double course = sc.nextDouble();
        System.out.println(String.format("При обмене вы получите %.2f €", rubles/course));
    }
}
