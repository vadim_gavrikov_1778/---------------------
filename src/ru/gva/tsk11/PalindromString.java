package ru.gva.tsk11;

import java.util.Scanner;

/**
 * Напишите метод, который будет проверять является ли строка палиндромом
 * (одинаково читающееся в обоих направлениях).
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class PalindromString {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String string = sc.nextLine();
        if (check(string)==true){
            System.out.println("является");
        }else {
            System.out.println("не является");
        }
    }

    /**
     * Данный метод проверяет является ли строка палиндромом.
     *
     * @param convert строка
     * @return результат проверки истина или лож.
     */

    private static boolean check (String convert) {
        return convert.equals((new StringBuilder(convert)).reverse().toString());
    }

}
