package ru.gva.tsk15;

import java.util.Random;
import java.util.Scanner;

/**
 * В квадратной матрице [n][n] поменять столбцы и строки местами.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Array {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int length = sc.nextInt();
        int[][] array_1 = new int[length][length];
        int[][] array_2 = new int[length][length];
        filling(array_1);
        print(array_1);
        transposition(array_1,array_2);
        print(array_2);

    }

    /**
     * Метод для транспонации.
     *
     * @param array_1 двухмерный массив который нужно транспонировать.
     * @param array_2 двухмерный массив
     */
    private static void transposition(int[][] array_1, int[][] array_2) {
        for (int i = 0; i < array_1.length; i++) {
            for (int j = 0; j < array_1[i].length; j++) {
                array_2[i][j] = array_1[j][i];
            }
        }
    }

    /**
     * Метод вывода двухменого массива.
     *
     * @param array двухмерный массив
     */
    private static void print (int[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();


        }
    }

    /**
     * Метод для заполнения двухмерного массива.
     *
     * @param array двухмерный массив.
     */
    private static void filling(int[][] array) {
        Random rm = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = rm.nextInt(30);
            }


        }
    }
}
