package ru.gva.tsk9;

import java.util.Scanner;

/**
 * Создайте метод, который в качестве аргумента получает число
 * и полностью обнуляет столбец прямоугольной матрицы,
 * который соответствует заданному числу.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class ATwoDimensionalArray {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] array = new int[sc.nextInt()][sc.nextInt()];
        filling(array);
        print(array);
        zero(array);
        print(array);
    }

    /**
     * Данный метод заменяет в узаннном столбце двух-мерного массива все эго элементы на 0.
     *
     * @param array двухмерный массив.
     */
    private static void zero(int[][] array) {
        int number = sc.nextInt();
        for (int i = 0; i < array.length; i++) {
            array[i][number] = 0;
        }
    }

    /**
     * Метод для на экран монитора двух-мерного массива.
     *
     * @param array двухмерный массив.
     */
    private static void print(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Метод для заполнения двух-мерного массива.
     *
     * @param array двухмерный массив.
     */
    private static void filling(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = 4 + (int) (Math.random() * (27));
            }
        }
    }
}
