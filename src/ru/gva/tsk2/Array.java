package ru.gva.tsk2;

import java.util.Scanner;

/**
 * Напишите метод, который будет увеличивать заданный элемент массива на 10%.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Array {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        double[] array = new double[sc.nextInt()];
        for (int i = 0; i <array.length; i++) {
            array[i] = 1 + (int) (Math.random()*(50));
            System.out.print(array[i] + " ");

        }
        int index = sc.nextInt()-1;
        array[index]=tenpercent(array[index]);
        System.out.println();
        System.out.println(array[index]);
    }

    /**
     * Метод который увеличивает указанный элемент массива на 10 процентов.
     *
     * @param v элемент массива
     * @return элемент массива увеличеный на 10%.
     */
    private static double tenpercent(double v) {
       return v + v/100*10;
    }
}
